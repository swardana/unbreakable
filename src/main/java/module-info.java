/*
 * Unbreakable is a J-Novel Club premium member assistant.
 * Copyright (C) 2024  Sukma Wardana
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * The unbreakable module descriptor.
 *
 * @moduleGraph
 *
 * @author Sukma Wardana
 * @since 1.0
 */
module com.swardana.unbreakable {

	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;

}
